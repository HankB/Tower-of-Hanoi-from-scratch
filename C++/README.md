# Tower of Hanoi / C++

## Motivation

* C++ classes data structures and algorithms. (C is not a new language for the author but changes in C++ since it was first standardized are less familiar.)
* C++ unit tests
* Dig into Cmake/Ctest. 

## Plan

* Identify test framework(s) to use.
* Describe data structures to use.
* Start coding and testing and coding and testing ... ;)
* Initial coding will technically be using C++ but will be restricted to the C compatible subset. Certain C++ constructs such as `static const int ...` Will be preferred to the C `#define`

## Building

```bash
mkdir C++/build
cd C++/build
cmake ..
make
ctest
```

## Organization

(At present)

* `C++/inc/tower.h` - Declarations for application
* `C++/src/catch2_Main.cpp` - Application main
* `C++/src/stack.cpp` - Implementation of Tower of Hanoi logic related to disk stacks
* `C++/src/board.cpp` - Implementation of Tower of Hanoi logic related to the board (3 stacks.)
* `C++/src/test-stack.cpp` - Implementation of Catch2 unit tests
* `C++/src/test-board.cpp` - Implementation of Catch2 unit tests

## Usage

`tower n` where 'n' is some integer 1-?.

(Command line arg not yet implemented.)

## Status

Working solution coded. `valgrind test_board` will fail until a cleanup routine is written
to release memory allocated for the tree of possible solutions. (See TODO)

## TODO - possible improvements

* Find an optimal solution. TRhe minimum number of moves required is 2^height - 1. A 5 disk stack should be solvable in 31 moves. This solution takes 81 steps.
* Cleanup memory before exiting. Since it is released anyway at exit this seems not required.

## Environment and requirements

Development and testing is performed on a variety of hosts running Debian 9, Debian testing and Ubuntu 16.04 and 18.04. `git` and the C/C++ development tools are required. GNU C/C++ is used. `valgrind` also needs to be installed. On Debian/Ubuntu the requirements can be installed with `sudo apt install valgrind g++ cmake`.

* Catch2 single header (installed along with the license file in C++/inc.)

## Data Structures

* `disk` - Represent a disk by a single non-zero integer where the relative
size of the integer indicates the relative size of the disk. In other words,
the disks in a 7 disk set are 1, 2, 3, 4, 5, 6, 7.
* `disk_stack` - A stack of disks represented as integers and
ordered from bottom to top. For example 3, 2, 1.
* `stack_set` - Set of three `disk_stack`s that represent the pegs on the
puzzle. These will be identified as `peg_a`, `peg_b` and `peg_c`.
* `previous_stack_set` - a link to the representation of the previous stack
set.
* `next_stack_set` - links to up to two next possible configurations.
(There are a total possible combination of 6 moves but half would not be legal
due to the size constraint. One of the remaining ones can be ruled out as
reversing the previous move.)

## Functionality

* `move()` - Move a disk to another peg.
* `valid()` - Test to see if a move is valid.
* `exists()` - iterate over all moves to see if the resulting `stack_set`
already exists.
* `connect()` - Connect a new stack_set to an existing stack_set (or
containers of some sort which hold the stack sets.)

## Testing

CTest seems to be "black box" in that the example I have validates
program output following execution. This is set aside in favor of a
more white box approach provided by Catch2. Rather than constructing
an executable for each function (or using command line arguments to
select specific tests) functions can be tested individually. Both
Catch2 and CTest are employed. Catch2 is used to unit test. CTest
runs the tests and also runs a `valgrind` test. Various incantations
(from `C++/build`) include:
```bash
./test_stack            # run Catch2 tests
./test_board            # run Catch2 tests
valgrind ./test_stack   # run valgrind test
valgrind ./test_board   # run valgrind test
ctest                   # run all tests
ctest -L StackTest      # all stack related tests
ctest -L BoardTest      # all board related tests
```

## Errata

One complaint about Cmake is the dearth of good documentation. 
I concur. The tutorial at <https://cmake.org/cmake-tutorial/> seems to provide 
a simple start but then neglects to include the commands required to build the
project. Unfortunately third party tutorials can be full of errors or just
incomplete. The best tutorial I have found so far is at
<http://derekmolloy.ie/hello-world-introductions-to-cmake/>. While not without
error, at least the associated git repo includes buildable projects. For the
purposes of this project, the second organization (Multi-directory Project)
seems most appropriate here.

Consider adopting a different unit test framework. Catch2 cannot test asserts. Or perhaps convert asserts to something better that Catch2 would support.

## Epilogue

I thought solving this in C would be a walk in the park. It was not. I tried
to use references thinking they were the C equivalent of safe pointers. They
are not. I learned a thing or two about references. I also felt throughout
the coding that this really wanted to be written in C++. Perhaps a future
variant... (I think Rust is going to be the next effort.)