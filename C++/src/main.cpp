#include <stdlib.h>
#include <stdio.h>

#include "tower.h"

int main(int argc, char** argv )
{
    const int s_b[] = {0};
    int height = 5; // default value
    int disks[max_height+1];
    const int* const stacks[] = { disks, s_b, s_b};

    if(argc > 1) {
        height = atoi(argv[1]);
        if(height<= 0 || height > max_height) {
            printf("Usage %s n (1 <= n <= %d)\n", argv[0], max_height);
            exit(1);
        }
    }

    for(int i=0; i <= height; i++) {
        disks[i] = height-i;
    }

    struct board *brd = make_board(stacks);
    solve(*brd);
    destroy_board(brd);

    return 0;
}