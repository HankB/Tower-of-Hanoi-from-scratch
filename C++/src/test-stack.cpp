#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include "tower.h"

#include "catch.hpp"

TEST_CASE("struct stack", "[test_tower]") {
    struct stack s;
    struct stack sCopy;
    const int disks[] = {3,2,1,0};
    const int smallerDisks[] = {2,1,0};
    static const int printBufLen=100;
    char    printBuf[printBufLen];


    populate_stack(s, disks);
    REQUIRE( memcmp(disks, s.disks, sizeof(disks)) == 0 );
    snprintf_stack(printBuf, printBufLen, s);
    REQUIRE( strcmp(printBuf, "  3  2  1") == 0 );
    
    copy_stack(sCopy, s);
    REQUIRE( memcmp(disks, s.disks, sizeof(disks)) == 0 );
    snprintf_stack(printBuf, printBufLen, sCopy);
    REQUIRE( strcmp(printBuf, "  3  2  1") == 0 );

    snprintf_stack(printBuf, 5, sCopy);     // small buffer
    REQUIRE( strcmp(printBuf, "  3 ") == 0 );

    snprintf_stack(printBuf, 4, sCopy);     // smaller buffer
    REQUIRE( strcmp(printBuf, "  3") == 0 );

    populate_stack(s, smallerDisks);
    REQUIRE( memcmp(smallerDisks, s.disks, sizeof(smallerDisks)) == 0 );
    snprintf_stack(printBuf, printBufLen, s);
    REQUIRE( strcmp(printBuf, "  2  1") == 0 );

    CHECK(match_stack(s, sCopy) == false);
    copy_stack(s, sCopy);
    CHECK(match_stack(s, sCopy) == true);

    const int d[] = {2,0};

    populate_stack(s, d);
    CHECK(match_stack(s, sCopy) == false);
    populate_stack(sCopy, d);
    CHECK(match_stack(s, sCopy) == true);

    int d2[] = {0};

    populate_stack(s, d2);
    CHECK(match_stack(s, sCopy) == false);
    populate_stack(sCopy, d2);
    CHECK(match_stack(s, sCopy) == true);
    snprintf_stack(printBuf, printBufLen, s);
    REQUIRE( strcmp(printBuf, "") == 0 );

    const int dLong[] = {10,9,8,7,6,5,4,3,2,1,0};

    populate_stack(s, dLong);
    CHECK(match_stack(s, sCopy) == false);
    populate_stack(sCopy, dLong);
    CHECK(match_stack(s, sCopy) == true);
    snprintf_stack(printBuf, printBufLen, s);
    REQUIRE( strcmp(printBuf, " 10  9  8  7  6  5  4  3  2  1") == 0 );

    const int dTooLong[] = {11,10,9,8,7,6,5,4,3,2,1,0};

    populate_stack(s, dTooLong);
    CHECK(match_stack(s, sCopy) == false);
    populate_stack(sCopy, dTooLong);
    CHECK(match_stack(s, sCopy) == true);
    snprintf_stack(printBuf, printBufLen, s);
    REQUIRE( strcmp(printBuf, " 11 10  9  8  7  6  5  4  3  2") == 0 );

    // not supported by Catch2 REQUIRE_ABORT(snprintf_stack(printBuf, 0, sCopy));

}

TEST_CASE("struct stack - height_stack()", "[test_tower]") {
    struct stack s;
    const int disks[] = {3,2,1,0};
    const int smallerDisks[] = {2,1,0};
    const int noDisks[] = {0};
    const int dLong[] = {10,9,8,7,6,5,4,3,2,1,0};
    const int dTooLong[] = {11,10,9,8,7,6,5,4,3,2,1,0};

    populate_stack(s, disks);
    CHECK(height_stack(s) == 3);

    populate_stack(s, smallerDisks);
    CHECK(height_stack(s) == 2);

    populate_stack(s, noDisks);
    CHECK(height_stack(s) == 0);

    populate_stack(s, dLong);
    CHECK(height_stack(s) == 10);

    populate_stack(s, dTooLong);
    CHECK(height_stack(s) == 10);

}

TEST_CASE("struct stack - push_stack()", "[test_tower]") {
    int d1[] = {5,0};
    int d2[] = {0};
    struct stack s1;
    struct stack s2;

    populate_stack(s1, d1);
    populate_stack(s2, d2);

    CHECK(height_stack(s1) == 1);
    CHECK(height_stack(s2) == 0);

    push_stack(s2, max_height);
    CHECK(height_stack(s2) == 1);

    populate_stack(s2, d2);
    push_stack(s2, 5);
    CHECK(height_stack(s2) == 1);
    CHECK(match_stack(s1, s2) == true);
    // not supported by Catch2 REQUIRE_ABORT(push_stack(s2, 6)); // larger disk
    push_stack(s2, 4);
    CHECK(match_stack(s1, s2) == false);

    populate_stack(s2, d2);
    for(int i=11; i>1; i--)
        push_stack(s2, i);
    // not supported by Catch2 REQUIRE_ABORT(push_stack(s2, 1)); // max height?
    CHECK(height_stack(s2) == 10);

    // not supported by Catch2 REQUIRE_ABORT(push_stack(s1, 0)); // invalid disk
}

TEST_CASE("struct stack - pop_stack()", "[test_tower]") {
    struct stack s;
    const int disks[] = {3,2,1,0};

    populate_stack(s, disks);
    for(int i=1; i<=3; i++) {
        CHECK(pop_stack(s) == i);
        CHECK(height_stack(s) == 3-i);
    }
    CHECK(height_stack(s) == 0);
    // not supported by Catch2 REQUIRE_ABORT(pop_stack(s)); // pop empty stack

}

TEST_CASE("struct stack - get_top_stack()", "[test_tower]") {
    struct stack s;
    const int disks[] = {5,0};

    populate_stack(s, disks);
    CHECK(get_top_stack(s) == 5);
    pop_stack(s);
    // not supported by Catch2 REQUIRE_ABORT((get_top_stack(s) == 5)); // top of empty stack
    for(int i=5; i>0; i--)
    {
        push_stack(s, i);
        CHECK(get_top_stack(s) == i);
    }
}
