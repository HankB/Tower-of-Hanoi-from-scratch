// A simple program that computes the square root of a number
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <memory.h>
#include <assert.h>

#include "tower.h"
/**
 * @file Stack related code.
 * 
 * Code used to create and manipulate stacks of disks
 * 
 */

/**
 * @brief Make a copy of a stack of disks (struct stack)
 * 
 * @param to Destination struct stack
 * @param frm Source struct stack
 */
void copy_stack(struct stack &to, const struct stack &frm)
{
  memcpy(&to, &frm, sizeof(struct stack));
}
/**
 * @brief Copy array of ints into a disk stack
 * 
 * @param to  disk stack to populate
 * @param disks array of ints representing disks
 * 
 * TODO: Validate array of ints
 */
void populate_stack(struct stack &to, const int disks[])
{
  memset(to.disks, '\0', sizeof to.disks);
  for(int i=0; i<max_height && disks[i] > 0; i++)
    to.disks[i] = disks[i];
}

/**
 * @brief Implement snprintf for a struct stack
 * 
 * @param buf Buffer for output
 * @param len Length of buf
 * @param st struct stack to print.
 */
void snprintf_stack(char* buf, const size_t len, const struct stack &st)
{
  assert(len > 0);
  buf[0] = '\0';
  // build string
  for(int i=0; st.disks[i] != 0 && i < max_height; i++)
  {
    snprintf(buf+strlen(buf), len-strlen(buf), " %2d", st.disks[i]);
  }
}

/**
 * @brief Compare two struct stacks to see if they match.
 * 
 * @param s1 first stack to compare
 * @param s2 second stack to compare
 * @return true if stacks match
 * @return false if stacks don't match
 */
bool match_stack(const struct stack &s1, const struct stack &s2)
{
  for(int i=0; i<max_height+1; i++) {
    if(s1.disks[i] == 0 && s2.disks[i] == 0)
      return true;
    if(s1.disks[i] != s2.disks[i])
      return false;
  }
  assert(false); // should not get here
  return false;
}

/**
 * @brief count disks on the struct stack
 * 
 * @param s struct stack for which disk count is returned
 * @return int disks on the struct stack
 */
int height_stack(const struct stack &s)
{
  for(int i=0; i<max_height+1; i++)
    if(s.disks[i] == 0)
      return i;
  assert(false); // should not get here
  return 0;
}

/**
 * @brief Add a disk to the top of the stack
 * 
 * @param s 
 */
void push_stack(struct stack &s, int disk)
{
  int height=height_stack(s);
  assert(disk > 0);                 // Valid disk??
  assert(height < max_height);      // room for one more disk?
  assert(height>0 ? s.disks[height-1] > disk : true); // trying to add a larger disk?
  s.disks[height] = disk;
  s.disks[height+1] = 0;
}

/**
 * @brief remove the last element from the struct stack and return it
 * 
 * @param s struct stack to remove/return last element
 * @return int last element that was on stack
 * 
 * Note: Stack must have at least one element to remove
 */
int pop_stack(struct stack &s)
{
  assert(height_stack(s)>0);
  int retVal = s.disks[height_stack(s)-1];
  s.disks[height_stack(s)-1] = 0;
  return retVal;
}

/**
 * @brief Get the top disk on struct stack
 * 
 * @param s non-empty struct stack
 * @return int disk on top of stack
 */
int get_top_stack(const struct stack &s)
{
  assert(height_stack(s)>0);
  return s.disks[height_stack(s)-1];
}
