#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include "tower.h"

#include "catch.hpp"


TEST_CASE("struct board - make_board()", "[test_tower]") {
    const int a_s[] = {5,4,3,0};
    const int b_s[] = {0};
    const int c_s[] = {2,1,0};
    const int* const stacks[] = { a_s, b_s, c_s};
    struct stack st[3];

    struct board *brd = make_board(stacks);
    for( int i=0; i<3; i++) {
        populate_stack(st[i], stacks[i]);
        CHECK(match_stack(st[i], brd->stacks[i]) == true);
    }
    destroy_board(brd);
}

TEST_CASE("struct board - match_board()", "[test_tower]") {
    const int a_s[] = {5,4,3,0};
    const int b_s[] = {6};
    const int c_s[] = {2,1,0};
    const int* const stacks[] = { a_s, b_s, c_s};

    struct board *brd_1 = make_board(stacks);
    struct board *brd_2 = make_board(stacks);

    CHECK(match_board(*brd_1, *brd_2) == true);
    for(int i=0; i<3; i++) {
        int d = pop_stack(brd_2->stacks[i]);
        CHECK(match_board(*brd_1, *brd_2) == false);
        push_stack(brd_2->stacks[i], d);
        CHECK(match_board(*brd_1, *brd_2) == true);
    }

    destroy_board(brd_1);
    destroy_board(brd_2);
}

TEST_CASE("struct board - can_move_disk_board()", "[test_tower]") {
    const int s_a[] = {5,4,3,0};
    const int s_b[] = {0};
    const int s_c[] = {2,1,0};
    const int* const stacks[] = { s_a, s_b, s_c};

    struct board *brd = make_board(stacks);
    CHECK(can_move_disk_board(*brd, a, b) == false);
    CHECK(can_move_disk_board(*brd, a, c) == true);
    CHECK(can_move_disk_board(*brd, b, c) == true);
    CHECK(can_move_disk_board(*brd, b, a) == true);
    CHECK(can_move_disk_board(*brd, c, a) == false);
    CHECK(can_move_disk_board(*brd, c, b) == false);

    destroy_board(brd); // comment out to force valgrind LEAK SUMMARY: definitely lost: ...

    const int* const stacks_2[] = { s_a, s_b, s_b};
    brd = make_board(stacks_2);
    CHECK(can_move_disk_board(*brd, a, b) == false);
    CHECK(can_move_disk_board(*brd, a, c) == false);
    CHECK(can_move_disk_board(*brd, b, c) == false);
    CHECK(can_move_disk_board(*brd, b, a) == true);
    CHECK(can_move_disk_board(*brd, c, b) == false);
    CHECK(can_move_disk_board(*brd, c, a) == true);
    CHECK(can_move_disk_board(*brd, c, b) == false);

    destroy_board(brd); // comment out to force valgrind LEAK SUMMARY: definitely lost: ...
}

TEST_CASE("struct board - snprintf_board()", "[test_tower]") {
    const int s_a[] = {5,4,3,0};
    const int s_b[] = {0};
    const int s_c[] = {2,1,0};
    const int dLong[] = {10,9,8,7,6,5,4,3,2,1,0};
    const int* const stacks[] = { s_a, s_b, s_c};
    const int* const long_stack[] = { dLong, s_b, s_b};
    const int* const full_stacks[] = { dLong, dLong, dLong};
    const int buflen = 200;
    char buf[buflen];

    struct board *brd = make_board(stacks);
    brd->from = u;
    brd->to = u;
    snprintf_board(buf, buflen, *brd);
    REQUIRE( strcmp(buf, 
        " ->   5  4  3                      |"
            "                               |"
            "  2  1                        ")
         == 0 ); 

    move_disk_board(*brd, b, a);
    brd->from = a;
    brd->to = b;
    snprintf_board(buf, buflen, *brd);
    REQUIRE( strcmp(buf, 
        "a->b  5  4                         |"
            "  3                            |"
            "  2  1                        ")
         == 0 ); 

    destroy_board(brd);

    brd = make_board(long_stack);
    brd->from = u;
    brd->to = u;
    snprintf_board(buf, buflen, *brd);
    REQUIRE( strcmp(buf, 
        " ->  10  9  8  7  6  5  4  3  2  1 |"
            "                               |"
            "                              ")
         == 0 );

    destroy_board(brd);

    brd = make_board(full_stacks);
    brd->from = c;
    brd->to = b;
    snprintf_board(buf, buflen, *brd);
    REQUIRE( strcmp(buf, 
        "c->b 10  9  8  7  6  5  4  3  2  1 |"
            " 10  9  8  7  6  5  4  3  2  1 |"
            " 10  9  8  7  6  5  4  3  2  1")
         == 0 );

    destroy_board(brd);

}


TEST_CASE("struct board - move_disk_board()", "[test_tower]") {
    const int s_a[] = {5,4,3,0};
    const int s_b[] = {0};
    const int s_c[] = {2,1,0};
    const int* const stacks[] = { s_a, s_b, s_c};

    struct board *brd_1 = make_board(stacks);
    struct board *brd_2 = make_board(stacks);

    CHECK(match_board(*brd_1, *brd_2) == true);
    move_disk_board(*brd_1, b, a );
    CHECK(match_board(*brd_1, *brd_2) == false);
    move_disk_board(*brd_1, a, b );
    CHECK(match_board(*brd_1, *brd_2) == true);
    pop_stack(brd_2->stacks[c]);
    CHECK(match_board(*brd_1, *brd_2) == false);
    push_stack(brd_2->stacks[c], 1);
    CHECK(match_board(*brd_1, *brd_2) == true);
    move_disk_board(*brd_1, a, c );
    CHECK(match_board(*brd_1, *brd_2) == false);
    move_disk_board(*brd_2, a, c );
    CHECK(match_board(*brd_1, *brd_2) == true);

    destroy_board(brd_1); // comment out to force valgrind LEAK SUMMARY: definitely lost: ...
    destroy_board(brd_2);
}

TEST_CASE("struct board - link_boards()", "[test_tower]") {
    const int s_a[] = {5,4,3,0};
    const int s_b[] = {0};
    const int s_c[] = {2,1,0};
    const int* const stacks[] = { s_a, s_b, s_c};

    struct board *brd_1 = make_board(stacks);
    struct board *brd_2 = make_board(stacks);
    struct board *brd_3 = make_board(stacks);

    link_boards(*brd_1, *brd_2);
    CHECK(brd_1->child[0] == brd_2);
    CHECK(brd_1 == brd_2->parent);
    link_boards(*brd_1, *brd_3);
    CHECK(brd_1->child[1] == brd_3);
    CHECK(brd_1 == brd_2->parent);
    CHECK(brd_1->child[0] == brd_2);
    CHECK(brd_1 == brd_3->parent);


    destroy_board(brd_1); // comment out to force valgrind LEAK SUMMARY: definitely lost: ...
    destroy_board(brd_2);
}


TEST_CASE("struct board - is_complete()", "[test_tower]") {
    const int s_a[] = {5,4,3,0};
    const int s_b[] = {0};
    const int s_c[] = {2,1,0};
    const int* const stacks[] = { s_a, s_b, s_c};

    struct board *brd = make_board(stacks);

    CHECK(is_complete(*brd) == false);
    move_disk_board(*brd, b, c );
    CHECK(is_complete(*brd) == false);
    move_disk_board(*brd, a, c );
    CHECK(is_complete(*brd) == false);
    move_disk_board(*brd, a, b );
    CHECK(is_complete(*brd) == true);
    move_disk_board(*brd, b, a );
    move_disk_board(*brd, c, a );
    move_disk_board(*brd, c, b );
    pop_stack(brd->stacks[a]);
    pop_stack(brd->stacks[a]);
    pop_stack(brd->stacks[a]);
    CHECK(is_complete(*brd) == true);


    destroy_board(brd); // comment out to force valgrind LEAK SUMMARY: definitely lost: ...
}

TEST_CASE("struct board - copy_board()", "[test_tower]") {
    const int s_a[] = {5,4,3,0};
    const int s_b[] = {0};
    const int s_c[] = {2,1,0};
    const int* const stacks[] = { s_a, s_b, s_c};

    struct board *brd = make_board(stacks);
    brd->parent = (struct board *)5;
    brd->child[a] = (struct board *)5;
    brd->child[b] = (struct board *)5;
    brd->child[c] = (struct board *)5;
    struct board *new_brd = copy_board(*brd);
    CHECK(match_board(*brd, *new_brd) == true);
    CHECK(new_brd->parent == 0);
    CHECK(new_brd->child[a] == 0);
    CHECK(new_brd->child[b] == 0);
    CHECK(new_brd->child[c] == 0);

    destroy_board(brd);
    destroy_board(new_brd);
}

TEST_CASE("struct board - has_matching_children()", "[test_tower]") {
    const int s_a[] = {5,4,3,0};
    const int s_b[] = {0};
    const int s_c[] = {2,1,0};
    const int* const stacks[] = { s_a, s_b, s_c};
    //static const size_t buf_len = 100;
    //char buf[buf_len];

    struct board *brd_1 = make_board(stacks);
    struct board *brd_2 = copy_board(*brd_1);
    link_boards(*brd_1, *brd_2);
    CHECK(has_matching_children(*brd_2, *brd_1) == true);
    move_disk_board(*brd_2, b, a);
    CHECK(has_matching_children(*brd_2, *brd_1) == true);

    struct board *brd_3 = copy_board(*brd_2);
    move_disk_board(*brd_3, b, c);
    CHECK(has_matching_children(*brd_3, *brd_1) == false);
    link_boards(*brd_2, *brd_3);
    CHECK(has_matching_children(*brd_3, *brd_1) == true);
    struct board *brd_4 = copy_board(*brd_3);
    CHECK(has_matching_children(*brd_4, *brd_1) == true);
    move_disk_board(*brd_4, a, b);
    CHECK(has_matching_children(*brd_4, *brd_1) == false);

}

TEST_CASE("struct board - is_unique_board()", "[test_tower]") {
    const int s_a[] = {5,4,3,0};
    const int s_b[] = {0};
    const int s_c[] = {2,1,0};
    const int* const stacks[] = { s_a, s_b, s_c};

    struct board *brd_1 = make_board(stacks);
    struct board *brd_2 = copy_board(*brd_1);

    CHECK(match_board(*brd_1, *brd_2) == true);
    CHECK(is_unique_board(*brd_1, *brd_2) == false);
    CHECK(is_unique_board(*brd_2, *brd_1) == false);

    // make brd_2 different
    move_disk_board(*brd_2, b, a);
    CHECK(match_board(*brd_1, *brd_2) == false);
    CHECK(is_unique_board(*brd_1, *brd_2) == true);

    // make brd_2 a child of brd_1
    link_boards(*brd_1, *brd_2);

    CHECK(match_board(*brd_1, *brd_2) == false);
    CHECK(is_unique_board(*brd_1, *brd_2) == false);
    CHECK(match_board(*brd_1, *brd_2) == false);
    CHECK(is_unique_board(*brd_2, *brd_1) == false);

    // make brd_3 and make it different
    struct board *brd_3 = copy_board(*brd_2);
    move_disk_board(*brd_3, b, c);
    CHECK(is_unique_board(*brd_3, *brd_1) == true);
    CHECK(is_unique_board(*brd_3, *brd_2) == true);

    // make brd_3 a child of board_2
    link_boards(*brd_2, *brd_3);
    CHECK(is_unique_board(*brd_1, *brd_3) == false);
    CHECK(is_unique_board(*brd_1, *brd_2) == false);
    CHECK(is_unique_board(*brd_1, *brd_1) == false);

    CHECK(is_unique_board(*brd_2, *brd_3) == false);
    CHECK(is_unique_board(*brd_2, *brd_2) == false);
    CHECK(is_unique_board(*brd_2, *brd_1) == false);

    CHECK(is_unique_board(*brd_3, *brd_3) == false);
    CHECK(is_unique_board(*brd_3, *brd_2) == false);
    CHECK(is_unique_board(*brd_3, *brd_1) == false);

    destroy_board(brd_1);
    destroy_board(brd_2);
    destroy_board(brd_3);
}

TEST_CASE("struct board - unwind()", "[test_tower]") {
    const int s_a[] = {5,4,3,0};
    const int s_b[] = {0};
    const int s_c[] = {2,1,0};
    const int* const stacks[] = { s_a, s_b, s_c};

    struct board *brd_1 = make_board(stacks);
    struct board *brd_2 = copy_board(*brd_1);
    struct board *brd_3 = copy_board(*brd_1);

    // make brd_2 different
    move_disk_board(*brd_2, b, a);

    // make brd_2 a child of brd_1
    brd_2->parent = brd_1;
    brd_1->child[0] = brd_2;
    link_boards(*brd_1, *brd_2);

    // make brd_3 different
    move_disk_board(*brd_3, b, a);
    move_disk_board(*brd_3, b, c);

    // make brd_3 a child of board_2
    brd_2->child[1] = brd_3;
    brd_3->parent = brd_2;
    link_boards(*brd_2, *brd_3);

    CHECK(unwind(*brd_3) == 2);
    CHECK(unwind(*brd_2) == 1);
    CHECK(unwind(*brd_1) == 0);

    destroy_board(brd_1);
    destroy_board(brd_2);
    destroy_board(brd_3);
}

#if 1
TEST_CASE("struct board - solve()", "[test_tower]") {
    const int s_a[] = {5,4,3,0};
    const int s_b[] = {0};
    const int* const stacks[] = { s_a, s_b, s_b};

    struct board *brd = make_board(stacks);
    solve(*brd);
    destroy_board(brd);
}
#endif