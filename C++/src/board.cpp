// A simple program that computes the square root of a number
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <memory.h>
#include <assert.h>

#include "tower.h"

/**
 * @file Board related code.
 * 
 * Code used to create and manipulate collections of three stacks
 * that represent a game board.
 * 
 */

/**
 * @brief Factory for creating a board layout.
 *
 * @param s disks for stacks a, b and c
 * @return struct board* New board (unless malloc() returns 0)
 * 
 * Free the allocated memory by calling destroy_board()
 */
struct board *make_board(const int*const s[3])
{
  struct board *brd = (struct board *)malloc(sizeof(struct board));
  if (brd == 0)
    return 0;

  for (int i = a; i <= c; i++)
    populate_stack(brd->stacks[i], s[i]);

  brd->from = brd->to = u;
  memset(brd->child, '\0', sizeof(brd->child));
  brd->parent = 0;

  return brd;
}

/**
 * @brief Free memory used by a board created by make_board()
 * 
 * @param b Pointer to board to destroy
 * @return * void 
 */
void destroy_board(struct board *b)
{
  free(b);
}

/**
 * @brief Compare two boards to see if they have the same stack configuration
 * 
 * @param brd_1, brd2 boards to compare 
 * @return true if boards match
 * @return false if they differ
 * 
 * Bug: Boards that have the same stack configurations but on different pegs 
 *      will be evaluated as not matching.
 */
bool match_board(const struct board &brd_1, const struct board &brd_2)
{
  for(int i = a; i <= c; i++) {
    if(!match_stack(brd_1.stacks[i], brd_2.stacks[i])) {
      return false;
    }
  }
  return true;
}

/**
 * @brief Determine if a move is legal
 * 
 * @param brd struct board for which move is considered
 * @param to index of struct stack destination
 * @param from index of struct stack source
 * @return true if the move is legal
 * @return false if the move is not allowed.
 */
bool can_move_disk_board(const struct board &brd, enum stack_ID to, enum stack_ID from )
{
  // if the source peg has no disks, no move!
  if(height_stack(brd.stacks[from]) == 0)
    return false; // maybe candidate for an assert()
  // next check - move to an empty peg always legal
  // but makes no sense if the source would be left empty
  else if(height_stack(brd.stacks[to]) == 0)
    return true;
  // both pegs have disks, must compare the tops 
  else  if(get_top_stack(brd.stacks[to]) > get_top_stack(brd.stacks[from]))
    return true;
  else
    return false;
}

/**
 * @brief Move top disk from one board to another.
 * 
 * @param brd Board for which the disk is to be moved.
 * @param to index of struct stack destination
 * @param from index of struct stack source
 * 
 * Note: It is assumed that can_move_disk_board() has been called to validate the move
 */
void move_disk_board(struct board &brd, enum stack_ID to, enum stack_ID from )
{
  push_stack(brd.stacks[to], pop_stack(brd.stacks[from]));
  brd.from = from;
  brd.to = to;
}

/**
 * @brief print board info (move and configuration) to buffer
 * 
 * @param buf buffer
 * @param len size of buffer
 * @param brd board configuration to print
 * 
 * Pad the resulting string for uniform display of consecutive layouts
 */
char * snprintf_board(char* buf, const size_t len, const struct board &brd)
{
  const char peg[] = " abc";
  const int pad_len = max_height*3; // max_height*strlen("  n")
  char stack_buf[pad_len+1];
  static char pad_format[10];
  // format for padding based on stack height
  snprintf(pad_format, sizeof(pad_format), "%%-%ds", pad_len);
  // move that got to this configuration
  snprintf(buf, len, "%c->%c", peg[brd.from+1], peg[brd.to+1]);
  // Format strings, pad and add to the output string
  for(int s=0; s<3; s++) {
    snprintf_stack(stack_buf, sizeof(stack_buf), brd.stacks[s]);  // format
    assert(len-strlen(buf) > pad_len+1);
    snprintf(buf+strlen(buf), pad_len+1, pad_format, stack_buf);  // pad
    if(s < 2)
      snprintf(buf+strlen(buf), len-strlen(buf), " |");           // separator
  }
  return buf;
}

void link_boards(struct board &parent, struct board &child)
{
  child.parent = &parent;
  for(int i=0; i<children_size; i++) {
    if(parent.child[i] == 0) {
      parent.child[i] = &child;
      return;
    }
  }
  assert(false); // no empty slot for child!!!
}

/**
 * @brief Determine of configuration consists of all disks on one stack
 * 
 * @param brd configuration to consider
 * @return true if all disks on one peg
 * @return false if not yet finished
 */
bool is_complete(const struct board &brd)
{
  int empty=0;
  for(int i = a; i <= c; i++) {
    if(height_stack(brd.stacks[i]) == 0) {
      empty++;
    }
  }
    return empty>1?true:false;
}

/**
 * @brief Make a copy of the board (stacks only)
 * 
 * @param brd board to copy
 * @return struct board* copy
 * As with make_board(), destroy_board() can be called to free memory
 */

struct board *copy_board(const struct board &brd)
{
  struct board *new_brd = (struct board *)malloc(sizeof(struct board));
  if (new_brd == 0)
    return 0;

  *new_brd = brd;
  memset(new_brd->child, '\0', sizeof(new_brd->child));
  new_brd->parent = 0;
  return new_brd;
}

/**
 * @brief Determine if any children of given board match the candidate
 * 
 * @param candidate bioard to match
 * @param brd_in_tree board w/ possible matching children
 * @return true if a child matches candidate.
 * @return false if no child matches candidate.
 * 
 * Note: recursive
 */
bool has_matching_children(struct board &candidate, struct board &brd)
{
  // try to match children
  for(int i=0; i<children_size; i++) {
    if(brd.child[i] != 0) {
      if(match_board(*brd.child[i], candidate)) {
        return true;
      }
    }
  }
  // try to match children's children
  for(int i=0; i<children_size; i++) {
    if(brd.child[i] != 0) {
      if(has_matching_children(candidate, *brd.child[i])) {
        return true;
      }
    }
  }
  return false;
}

/**
 * @brief determine of the stack configuration exists anywhere in the tree
 * 
 * @param candidate Board to try to match
 * @param brd_in_tree Some board in the tree
 * @return true if candidate matches some board in the tree.
 * @return false If candidate does not match any board in the tree.
 */
bool is_unique_board(struct board &candidate, struct board &brd_in_tree)
{
  //const struct board& starting_point = brd_in_tree;
  struct board * brd = &brd_in_tree;

  // find our way to the top of the tree
  while(brd->parent != 0)
  {
    // should check for loops
    brd = brd->parent;
  }
  
  // test top of the tree
  if(match_board(*brd, candidate) || has_matching_children(candidate, *brd))
    return false;

  return true;
}

/**
 * @brief Unwind to top of tree and output results
 * 
 * @param brd final board configuration
 * @return number of steps to get to top 
 */
int unwind(struct board &brd)
{
  int steps = 0;
  struct board *parent_brd = &brd;
  static const size_t buf_len = 100;
  char buf[buf_len];

  while(parent_brd->parent != 0) {
    steps++;
    parent_brd = parent_brd->parent;
  }
  // now allocate an array to hold pointers to the results
  // so we can print in order
  struct board ** path = (struct board**) malloc((steps+1) * sizeof(struct board *));
  memset(path, 0, (steps+1) * sizeof(struct board *));
  steps = 0;
  parent_brd = &brd;
  while(parent_brd->parent != 0) {
    path[steps] = parent_brd;
    steps++;
    parent_brd = parent_brd->parent;
  }
  printf("%d steps to solve puzzle\n", steps);
  path[steps] = parent_brd;

  for(int i=steps; i>=0; i--) {
    printf("%s\n", snprintf_board(buf,buf_len, *path[i]));
  }

  return steps;
}


/**
 * @brief Solve the Tower of Hanoi from the given starting position
 * 
 * @param brd 
 * @return true 
 * @return false 
 */
bool solve(struct board &brd)
{
  int child_count = 0;

  for(int from=a; from <= c; from++) {  // iterate over all possible 'from' positions
    for(int to=a; to<=c; to++) {        // and all possible 'to' positions
      if(from == to)
        continue;
      if(can_move_disk_board(brd, (stack_ID)to, (stack_ID)from)) {
        // build child node and link to parent (this)
        struct board *new_brd = copy_board(brd); 
        assert(new_brd != 0);
        move_disk_board(*new_brd, (stack_ID)to, (stack_ID)from);
        if(is_unique_board(*new_brd, brd)) {
          brd.child[child_count] = new_brd;
          new_brd->parent = &brd;
          new_brd->from = (stack_ID)from;
          new_brd->to = (stack_ID)to;
          if(is_complete(*new_brd)) {   // is this a solution?
            // unwind path to starting point
            // First count the depth of recursion by backing up
            // the chain
            unwind(*new_brd);
            return true;
          }
          child_count++;
          assert(child_count<=children_size);
        }
        else {
          destroy_board(new_brd);
        }
      }
      else {
      }
    }
  }
  // iterate through children, looking for a solution
  for(int i=0; i<children_size; i++) {
    if(brd.child[i] != 0) {
      if(solve(*brd.child[i])) {
        return true;
      }
    }
  }
  return false;
}
