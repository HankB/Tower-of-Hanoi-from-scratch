// Include for C/C++ Tower of Hanoi solution
#ifndef TOWER_H
#define TOWER_H

/**
 * @brief Identify stacks within a Board
 * 
 * Note: Order of definition is important to correct operation of code
 * such as in make_board()
 */
enum stack_ID
{
    u = -1, // unused
    a,
    b,
    c
};

static const int max_height = 10;
static const int children_size = 3;

/**
 * @brief Represent a stack of disks
 * 
 * In the time honored tradition of C, the end of the stack is denoted
 * by a null terminator. The top of the stack is the last element in
 * the array.
 */
struct stack
{
    int disks[max_height+1];
};

/**
 * @brief Encapsulate data for a board
 * 
 */
struct board
{
    struct stack stacks[3];
    struct board * parent;
    struct board * child[children_size];    // at most three child configurations
    stack_ID    from;       // move that got to this position
    stack_ID    to;
};

void copy_stack(struct stack &to, const struct stack &frm);
void populate_stack(struct stack &to, const int disks[]);
void snprintf_stack(char* buf, const size_t len, const struct stack &st);
bool match_stack(const struct stack &s1, const struct stack &s2);
int height_stack(const struct stack &s);
void push_stack(struct stack &s, int disk);
int pop_stack(struct stack &s);
int get_top_stack(const struct stack &s);
struct board *make_board(const int*const s[3]);
bool match_board(const struct board &brd_1, const struct board &brd_2);
void destroy_board(struct board *);
//struct stack &get_stack_board(struct board *);
bool can_move_disk_board(const struct board &brd, enum stack_ID to, enum stack_ID from );
void move_disk_board(struct board &brd, enum stack_ID to, enum stack_ID from );
struct board *copy_board(const struct board &brd);
void link_boards(struct board &parent, struct board &child);
bool is_complete(const struct board &brd);
char * snprintf_board(char* buf, const size_t len, const struct board &brd);
bool solve(struct board &brd);
int unwind(struct board &brd);
bool has_matching_children(struct board &candidate, struct board &brd);
bool is_unique_board(struct board &candidate, struct board &brd_in_tree);

#endif // TOWER_H