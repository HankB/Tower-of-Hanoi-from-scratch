# Tower of Hanoi / Python

## Motivation

* Python data structures
* Python unit tests
* Other tests?

## Plan

* Identify test framework(s) to use.
* Describe data structures to use.
* Start coding and testing and coding and testing ... ;)

## Usage:

`tower.py n` where 'n' is an integer 1-6. Larger than 6 and the recursion limit is reached.

## Status

Complete.

## TODO - possible improvements

* Rewrite as non-recursive to avoid blowing up the stack on larger stacks of disks.
* Rework logic to find optimum solution. (Left as an exercise for the student?)
* Remove output from StackSet.solve() to allow for other display options.
* Better formatting of output.

## Environment and requirements

Development and testing is performed on a variety of hosts running Debian 9, Debian testing and Ubuntu 16.04. Except for `git` which may not be installed by default, no additional packages seem to be required.

## Datastructures

* `disk` - Represent a disk by a single non-zero integer where the relative
size of the integer indicates the relative size of the disk. In other words,
the disks in a 7 disk set are 1, 2, 3, 4, 5, 6, 7.
* `disk_stack` - A stack of disks represented as integers and
ordered from bottom to top. For example 3, 2, 1.
* `stack_set` - Set of three `disk_stack`s that represent the pegs on the
puzzle. These will be identified as `peg_a`, `peg_b` and `peg_c`.
* `previous_stack_set` - a link to the representation of the previous stack
set.
* `next_stack_set` - links to up to two next possible configurations.
(There are a total possible combination of 6moves but half would not be legal
due to the size constraint. One of the remaining ones can be ruled out as
reversing the previous move.)

## Functionality

* `move()` - Move a disk to another peg.
* `valid()` - Test to see if a move is valid.
* `exists()` - iterate over all moves to see if the resulting `stack_set`
already exists.
* `connect()` - Connect a new stack_set to an existing stack_set (or
containers of some sort which hold the stack sets.)

## Testing

The included unittest package is used.

In the Python3 directory run `./unittest_tower.py` to perform tests.
