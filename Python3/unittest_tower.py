#!/usr/bin/env python3
'''
Unit tests for the Tower of Hanoi puzzle
'''

import tower
import unittest
import inspect
import sys

def unittest_verbosity():
    """Return the verbosity setting of the currently running unittest
    program, or 0 if none is running.
    from https://stackoverflow.com/questions/13761697/
    """
    frame = inspect.currentframe()
    while frame:
        self = frame.f_locals.get('self')
        if isinstance(self, unittest.TestProgram):
            return self.verbosity
        frame = frame.f_back
    return 0

valid_stacks = ([1], [7,6,5,4,3,2], [7,6], [7,6,5], [])
invalid_stacks = ([1,2], [3,1,2], [7,6,6], [7,8,6], [8,7,8])

class DiskStackTest(unittest.TestCase):
    def testConstructor(self):

        verbosity=unittest_verbosity()

        # test all cases
        for i in valid_stacks + invalid_stacks:
            if verbosity > 1:
                print(i)
            t = tower.DiskStack(i)
            self.assertCountEqual(t.get_peg(), i)

    def testValidCheck(self):

        verbosity=unittest_verbosity()

        # test valid cases
        for i in valid_stacks:
            if verbosity > 1:
                print(i)
            t = tower.DiskStack(i)
            self.assertTrue(t.is_valid())

        # test invalid cases
        for i in invalid_stacks:
            if verbosity > 1:
                print(i)
            t = tower.DiskStack(i)
            self.assertFalse(t.is_valid())

    def testAccessors(self):

        verbosity=unittest_verbosity()

        for i in valid_stacks + invalid_stacks:
            if verbosity > 1:
                print(i)
            t = tower.DiskStack(i)
            if len(i) > 0:
                self.assertEqual(t.get_top(), i[-1], "get_top() wrong value")
            else:
                self.assertIsNone(t.get_top(), "get_top() should return 'None'")

    def testManipulators(self):

        verbosity=unittest_verbosity()

        t = tower.DiskStack([])
        with self.assertRaises(tower.IllegalOperationError):
            t.remove()
        t.add(3)
        with self.assertRaises(tower.IllegalOperationError):
            t.add(4)
        self.assertCountEqual(t.get_peg(), [3], "add(3) wrong result")
        self.assertEqual(t.remove(), 3, "remove() wrong value")
        self.assertCountEqual(t.get_peg(), [], "add(3) wrong result")

        t = tower.DiskStack([5,4,3])
        t.add(2)
        self.assertCountEqual(t.get_peg(), [5,4,3,2], "add(3) wrong result")
        self.assertEqual(t.remove(), 2, "remove() wrong value")
        self.assertCountEqual(t.get_peg(), [5,4,3], "add(3) wrong result")


class StackSetTest(unittest.TestCase):
    def testConstructor(self):

        verbosity=unittest_verbosity()

        board1 = tower.StackSet([3,2,1], [], [])
        self.assertCountEqual(board1.get_stack(0), [3,2,1])
        self.assertCountEqual(board1.get_stack(1), [])
        self.assertCountEqual(board1.get_stack(2), [])

        board2 = tower.StackSet([3,2,1], [4], [5])
        self.assertCountEqual(board2.get_stack(0), [3,2,1])
        self.assertCountEqual(board2.get_stack(1), [4])
        self.assertCountEqual(board2.get_stack(2), [5])

    def testComparator(self):
        board1 = tower.StackSet([3,2,1], [], [])
        board2 = tower.StackSet([3,2,1], [4], [5])

        self.assertTrue(board1.matches(board1))
        self.assertFalse(board1.matches(board2))

        board2 = tower.StackSet([4,2,1], [], [])
        self.assertFalse(board1.matches(board2))

        board2 = tower.StackSet([3,2,1], [4], [])
        self.assertFalse(board1.matches(board2))

        board2 = tower.StackSet([3,2,1], [], [4])
        self.assertFalse(board1.matches(board2))

    def testSprint(self):
        board1 = tower.StackSet([3,2,1], [], [])
        board2 = tower.StackSet([3,2], [1], [4])

        self.assertEqual(board1.sprint(), "0:[3, 2, 1]\t1:[]\t2:[]")
        self.assertEqual(board2.sprint(), "0:[3, 2]\t1:[1]\t2:[4]")

    def testMove(self):
        board1 = tower.StackSet([3,2,1], [], [])
        board2 = tower.StackSet([3,2], [], [4])

        self.assertTrue(board2.can_move(0,1))
        self.assertFalse(board2.can_move(1,0))
        self.assertTrue(board2.can_move(0,2))
        self.assertFalse(board2.can_move(2,0))
        self.assertFalse(board1.can_move(2,1))
        self.assertFalse(board1.can_move(1,2))

        self.assertEqual(board2.sprint(), "0:[3, 2]\t1:[]\t2:[4]")
        board2.move(0,1)
        self.assertEqual(board2.sprint(), "0:[3]\t1:[2]\t2:[4]")
        board2.move(1,2)
        self.assertEqual(board2.sprint(), "0:[3]\t1:[]\t2:[4, 2]")

        with self.assertRaises(tower.IllegalOperationError):
            board2.move(1,2)

    def testCopy(self):
        ''' do we need copy() functions?
        '''
        board1 = tower.StackSet([3,2,1], [], [])
        board2 = board1
        board2.move(0,1)
        self.assertEqual(board1.sprint(), board2.sprint()) # <sigh> Python does a shallow copy.
        board1.move(1,0)    # return to initial condition

        board2 = board1.copy()
        self.assertTrue(board1.matches(board2))
        board2.move(0,1)
        self.assertFalse(board1.matches(board2))

    def testLink(self):
        board1 = tower.StackSet([3,2,1], [], [])
        board2 = board1.copy()
        board2.move(0,2)

        board1.link(board2)
        self.assertEqual(board2.parent.sprint(), board1.sprint())
        self.assertEqual(board2.source_move, (0,2))
        self.assertEqual(board1.children[0], board2)


    def testMatching(self):

        board1 = tower.StackSet([3,2,1], [], []) # Start with one board
        board2 = board1.copy()
        tower.StackSet.reset_match_visits()
        self.assertTrue(board1.search_match(board1))
        self.assertEqual(tower.StackSet.get_match_visits(), 1)

        # test match with one board that matches first board in tree
        board1.link(board2)
        tower.StackSet.reset_match_visits()
        self.assertTrue(board1.search_match(board2))
        self.assertEqual(tower.StackSet.get_match_visits(), 1)  # match first check

        # test match with one board that matches second board in tree
        board2.move(0,2)
        tower.StackSet.reset_match_visits()
        self.assertTrue(board1.search_match(board2))
        self.assertEqual(tower.StackSet.get_match_visits(), 2)  # match second check

        # Third board that doesn't match any in tree
        board3 = board1.copy()
        board3.move(0,1)
        tower.StackSet.reset_match_visits()
        self.assertFalse(board1.search_match(board3))
        self.assertEqual(tower.StackSet.get_match_visits(), 2)  # match second check

        # Link third board that doesn't match any in tree
        board2.link(board3)
        tower.StackSet.reset_match_visits()
        self.assertTrue(board1.search_match(board3))
        self.assertEqual(tower.StackSet.get_match_visits(), 3)  # match third check

        # Another board that doesn't match the other three
        board4 = board1.copy()
        board4.move(0,2)
        board4.move(0,1)
        board4.move(2,1)
        tower.StackSet.reset_match_visits()
        self.assertFalse(board1.search_match(board4))
        self.assertEqual(tower.StackSet.get_match_visits(), 3)  # check all members

        # Add some dimensionality :)
        board1.link(board4)
        tower.StackSet.reset_match_visits()
        self.assertTrue(board1.search_match(board4))

        # Another board that doesn't match any in the tree
        board5 = board1.copy()
        board5.move(0,2)
        board5.move(0,1)
        board5.move(2,0)
        tower.StackSet.reset_match_visits()
        self.assertFalse(board1.search_match(board5))
        self.assertEqual(tower.StackSet.get_match_visits(), 4)  # test against all members

    def testSolving(self):
        ''' test routines related to solving '''
        board1 = tower.StackSet([3,2,1], [], []) # Start with one board

        self.assertTrue(board1.is_solution())

        board1.move(0,1)
        self.assertFalse(board1.is_solution())

        board2 = tower.StackSet([], [], []) # Start with empty board
        with self.assertRaises(tower.IllegalOperationError):
            board2.set_start()

        self.assertEqual(None, tower.StackSet.starting_point)
        board1.set_start()
        self.assertEqual(board1, tower.StackSet.starting_point)

        # Not sure how to test the solution itself - just run some scenarios
        # and verify that they finish. Manually duplicate the moves to see
        # if they are correct.
        board1.solve()

        board1 = tower.StackSet([3,2,1], [], []) # Start with one board
        board1.set_start()
        board1.solve()

        board2 = tower.StackSet([5,4,3,2,1], [], []) # Start with empty board
        board2.set_start()
        board2.solve()


        sys.setrecursionlimit(1500) # default is 1000 and results in
            # "RecursionError: maximum recursion depth exceeded in comparison"
        board2 = tower.StackSet([7,6,5,4,3,2,1], [], []) # Start with empty board
        board2.set_start()
        board2.solve()

if __name__ == "__main__": 
    unittest.main()


