#!/usr/bin/env python3
'''
Solve the Tower of Hanoi puzzle
'''

'''
Possible errors
'''
class Error(Exception):
    """Base class for exceptions in this module."""
    pass

class IllegalOperationError(Error):
    """Exception raised for errors in the input.

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message):
        self.message = message

'''
Encapsulate a single stack of disks.
'''
class DiskStack:
    def __init__(self, disks=[]):
        self.peg=disks.copy()
        return

    def get_peg(self):
        return self.peg

    def is_valid(self):
        if len(self.peg) == 0 or len(self.peg) == 1:
            return True
        else:
            for i in range(1,len(self.peg)):
                if(self.peg[i] >= self.peg[i-1]):
                    return False

        return True

    def get_top(self):
        if len(self.peg) > 0:
            return self.peg[-1]
        else:
            return None

    def remove(self):
        if len(self.peg) > 0:
            return self.peg.pop()
        else:
            raise IllegalOperationError("cannot remove disk: peg empty")

    def add(self, disk):
        if len(self.peg) == 0 or disk < self.get_top():
            self.peg.append(disk)
        else:
            raise IllegalOperationError("cannot add disk: "+str(disk)+">"+str(self.get_top()))

        return

'''
Encapsulate a game board
3 stacks of disks
links to possible next configuration
'''
class StackSet:
    def __init__(self, a=[], b=[], c=[]):
        self.stacks=[DiskStack(a), DiskStack(b), DiskStack(c)]
        self.parent=None        # predecessor to this configuration
        self.source_move=None   # Move to get from parent to self
        self.children=[]      # Configurations for which self is parent

    def get_stack(self, ix):
        return self.stacks[ix].get_peg()

    def matches(self, other):
        for i in range(0,3):
            if other.get_stack(i) != self.get_stack(i):
                return False
        return True

    def can_move(self, frm, to):
        if (self.stacks[frm].get_top() != None
            and (self.stacks[to].get_top() == None
                or (self.stacks[to].get_top() >
                    self.stacks[frm].get_top()))):
            return True
        return False

    def sprint(self):
        buf = ( "0:"+str(self.get_stack(0))+"\t"+
                "1:"+str(self.get_stack(1))+"\t"+
                "2:"+str(self.get_stack(2)))
        return buf

    def move(self,frm,to):
        self.stacks[to].add(self.stacks[frm].remove())
        self.source_move = (frm, to)
        return

    def copy(self):
        ''' Return a new StackSet with the same pegs.
        '''
        return StackSet(self.get_stack(0), self.get_stack(1), self.get_stack(2))

    def link(self,child):
        self.children.append(child)
        child.parent = self

    def visit_children(self, callback):
        ''' This seems generally not useful '''
        for child in self.children:
            callback(child)

    match_visit_count=0
    def get_match_visits():
        return StackSet.match_visit_count

    def reset_match_visits():
        StackSet.match_visit_count = 0

    def check_matches(self, child):
        if self.matches(child):
            return True
        else:
            return False

    def search_match(self, possible):
        ''' recursively search through the start (self) and children to see if any
        match  'possible'. This search will proceed depth first. '''
        StackSet.match_visit_count += 1
        if self.check_matches(possible):    # Match this node?
            return True
        else:
            for child in self.children:     # Then proceed to check children
                if child.search_match(possible):
                    return True
        return False

    def is_solution(self):
        ''' Are all all disks on one peg? Return True if so
        (or False if this is not a solution)
        '''
        empty_stack_count=3;
        stack_full = None
        for peg in self.stacks:
            if len(peg.get_peg()) > 0:
                empty_stack_count -= 1
        if empty_stack_count == 2:
            return True
        else:
            return False

    starting_point = None
    def set_start(self):
        disk_count=0
        for peg in self.stacks:
            disk_count += len(peg.get_peg())
        if disk_count == 0:
            StackSet.starting_point=None
            raise IllegalOperationError("All stacks on board are empty")

        StackSet.starting_point=self
        print("Starting at", self.sprint())

    def solve(self):
        ''' Solving takes two stages: 
            1) Search until the solution is found
            2) Work back up the chain to report the steps that got to the solution

        Solving involves:
            1) For a given node, identify all possible next moves. If one
               is a solution (all disks on other than the starting peg) Done!
            2) Link in next moves and see if any of them lead to a solution.
        '''
        if StackSet.starting_point == None:
            raise IllegalOperationError("cannot without starting point 'set_start()")

        for stack in self.stacks:
            # try all moves, link in valid next options
            for source in range(0,3):
                for dest in range(0,3):
                    if source == dest:
                        continue
                    if self.can_move(source, dest):
                        new_set = self.copy()
                        new_set.move(source, dest)
                        if StackSet.starting_point.search_match(new_set) == False: # not looping back?
                            #print(new_set.source_move, "linking", new_set.sprint(), "to", self.sprint())
                            self.link(new_set)
                            if new_set.is_solution():
                                # print("Solved!)", new_set.sprint(), new_set.parent)
                                # ... unwind moves that got to the solution
                                moves = []
                                move_count = 0
                                while new_set.parent != None:
                                    #print(new_set.parent.source_move, "to",  new_set.sprint())
                                    moves.append((new_set.source_move, new_set.sprint()))
                                    new_set = new_set.parent
                                    move_count += 1
                                print('{} moves in order from {}'.format(move_count, new_set.sprint()))
                                for step in reversed(moves):
                                    print('{} {}'.format(step[0], step[1]))

                                return True

        for child in self.children:     # iterate through all children
            if child.solve() == True:
                return True

        return False

import sys

def usage():
    print("Usage: '{} [n]' where n is the stack size 1-6".format(sys.argv[0]))
    sys.exit()

def main():
    if len(sys.argv) == 2:
        try:
            stack_size = int(sys.argv[1])
        except:
            usage()
    elif len(sys.argv) != 1:
        usage()
    else:
        stack_size = 4

    board = StackSet(list(reversed(range(1,stack_size+1))), [], []) # Start with empty board
    board.set_start()
    try:
        board.solve()
    except:
        print("Cannot do more than 6 tall without expanding the stack")


main()