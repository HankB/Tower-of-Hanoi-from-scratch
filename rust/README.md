# Tower of Hanoi / Rust

## Motivation

* Explore a new language.

## Logic

Reprersent a stack (`Stack`) of game disks as a vector (`Vec`) of unsigned short ints (`u32`). The height of the stack is the number of disks on the `Stack`. There are associated functions for manipulating the disks on the stack as well as constructing a stack.

A game board consists of three `Stack`s.

## Status

* File/code reorganization complete.
* Stack struct coded and tested. (Zero DOC-tests at present.) Coulr probably use more thorough tests.
* Board struct started - constructor executes but needs more tests.

### TODO

* What is `Doc-tests`?
* Use (status, value) returns for moves vs. pre-checking move.

## Credits

Much help from redditor `/u/__fmease__` at https://www.reddit.com/r/learnrust/comments/ax19rz/managing_return_value_for_impl_fmtdisplay_for/

## Building

```bash
cd .../Tower-of-Hanoi-from-scratch/rust/tower
cargo build
```

## Requirements

Install `rust` and `cargo` by desired means. (See "https://rustup.rs/".)
Your distro may have it packaged. Debian/Testing had `rustc` and `cargo`
but not `rustup`. Debian/Stretch had none.

## Organization

* `src/stack.rs` Code related to a stack of disks
* `src/board.rs` Code related to a made up of three stacks
* `src/main.rs` At present pulls in modules stack and board to facilitate testing.

## Testing

```bash
cd .../Tower-of-Hanoi-from-scratch/rust/tower
cargo test
cargo test stack
cargo test board -- --nocapture # reveal all of my debug print!() statements ;)
RUSTFLAGS="$RUSTFLAGS -A warnings" cargo test # temporarily suppress warnings
```

## Errata

`Stack::pop()` and `Stack::push()` can fail. Present code assumes working
code will check ahead of time before calling with with problem values. It
might make more sense to have these return some kind of `Result` that would
indicate success/failure and streamline the logic for the caller.