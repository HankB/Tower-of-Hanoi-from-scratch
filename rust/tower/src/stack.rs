use std::fmt;
use std::cell::RefCell;

// pub const MAX_HEIGHT: usize = 10;

pub struct Stack {
    disks: Vec<u32>,
}
thread_local! {
    pub static CAPACITY: RefCell<usize> = RefCell::new(5);
}

impl Stack {
    pub fn new(d: &Vec<u32>) -> Stack {
        let s = Stack { disks: (*d).to_vec() };
        s
    }

    pub fn set_capacity(c: usize) {
        CAPACITY.with(|capacity| { *capacity.borrow_mut() = c});
    }

    #[allow(dead_code)]
    pub fn get_capacity() -> usize {
        CAPACITY.with(|capacity| { *capacity.borrow_mut()})
   }

    pub fn height(&self) -> usize {
        self.disks.len()
    }

    pub fn top(&self) -> Option<u32> {
        if self.height() > 0 {
            Some(self.disks[self.disks.len() - 1])
        } else {
            None
        }
    }

    pub fn push(&mut self, d: u32) {
        if self.disks.len() == Stack::get_capacity() {
            panic!("push() to full stack");
        }
        match self.top() {
            Some(t) => {
                if t > d {
                    self.disks.push(d);
                } else {
                    panic!("disk not smaller than top");
                }
            }
            None => {
                self.disks.push(d);
            }
        }
    }

    pub fn pop(&mut self) -> u32 {
        match self.disks.pop() {
            Some(t) => {
                return t;
            }
            None => {
                // TODO: Consider propagating a result
                panic!("pop from empty stack");
            }
        }
    }
}

impl fmt::Display for Stack {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for d in self.disks.iter() {
            write!(f, "{:3}", d).expect("Can't write");
        }
        // pad to maximum stack height
        let padding_needed: usize;
        padding_needed = (Stack::get_capacity() - self.disks.len()) * 3;
        write!(f, "{:width$}", "", width = padding_needed)
    }
}

#[cfg(test)]
#[allow(non_snake_case)]
mod tests {
    use crate::stack::Stack;
    #[test]
    fn test_Stack_new() {
        Stack::set_capacity(5);
        // build a stack and verify height.
        let b1:  Vec<u32> = [5, 4, 3, 2, 1].to_vec();
        let s1 = Stack::new(&b1);
        assert_eq!(s1.height(), b1.len());

        // 2nd stack likewise
        let b2:  Vec<u32> = [9, 8, 7, 6].to_vec();
        let s2 = Stack::new(&b2);
        assert_eq!(s2.height(), b2.len());
        // stacks aren't sharing data
        assert_eq!(s1.height(), b1.len());

        // empty stack
        let b3: Vec<u32> =  Vec::new();
        let s3 = Stack::new(&b3);
        assert_eq!(s3.height(), b3.len());
    }

    #[test]
    fn test_Stack_accessors() {
        Stack::set_capacity(5);
        // build a stack and verify height.
        let b1: Vec<u32> = [5, 4, 3, 2, 1].to_vec();
        let s1 = Stack::new(&b1);

        // 2nd stack likewise
        let b2: Vec<u32>  = [9, 8, 7, 6].to_vec();
        let s2 = Stack::new(&b2);

        // empty stack
        let b3: Vec<u32>  = Vec::new();
        let s3 = Stack::new(&b3);

        // test Stack::top()
        // top of b1
        match s1.top() {
            Some(t) => assert_eq!(t, b1[b1.len() - 1]),
            None => assert!(false),
        }

        // top of b2
        match s2.top() {
            Some(t) => assert_eq!(t, b2[b2.len() - 1]),
            None => assert!(false),
        }

        // b3 is empty
        match s3.top() {
            Some(_) => assert!(false),
            None => assert!(true),
        }
    }

    #[test]
    #[should_panic(expected = "pop from empty stack")]
    fn test_Stack_manipulators() {
        Stack::set_capacity(5);
        let b1: Vec<u32> = [5, 4, 3, 2].to_vec();
        let mut s1 = Stack::new(&b1);
        s1.push(1);

        // test Stack::pop()

        let b2: Vec<u32> = Vec::new();
        let mut s2 = Stack::new(&b2);
        s2.push(5);
        s2.push(4);
        s2.push(3);

        // test Stack::pop()

        s1 = Stack::new(&b1);
        for i in b1.iter().rev() {
            match s1.top() {
                Some(t) => {
                    assert_eq!(&t, i);
                    assert_eq!(t, s1.pop());
                }
                None => assert!(false),
            }
        }
        s1.pop();
    }

    #[test]
    #[should_panic(expected = "disk not smaller than top")]
    fn test_Stack_illegal_push_1() {
        Stack::set_capacity(5);
        let b1: Vec<u32> = [5, 4, 3, 2].to_vec();
        let mut s1 = Stack::new(&b1);
        s1.push(5);
    }

    #[test]
    #[should_panic(expected = "disk not smaller than top")]
    fn test_Stack_illegal_push_2() {
        Stack::set_capacity(5);
        let b1: Vec<u32> = [5, 4, 3, 2].to_vec();
        let mut s1 = Stack::new(&b1);
        s1.push(2);
    }

    #[test]
    #[should_panic(expected = "push() to full stack")]
    fn test_Stack_push_past_capacity() {
        Stack::set_capacity(4);
        //let b1: Vec<u32> = [5, 4, 3, 2].to_vec();
        let mut s1 = Stack::new(&[5, 4, 3, 2].to_vec());
        s1.push(1);
    }


    #[test]
    fn test_Stack_formatter() {
        Stack::set_capacity(5);
        let b1: Vec<u32> = [5, 4, 3, 2].to_vec();
        let s1 = Stack::new(&b1);
        assert_eq!(format!("{}", s1), "  5  4  3  2   ");

        let b3: Vec<u32> = Vec::new();
        let s3 = Stack::new(&b3);
        assert_eq!(format!("{}", s3), "               ");

        let b1: Vec<u32> = [5, 4, 3].to_vec();
        let s1 = Stack::new(&b1);
        assert_eq!(format!("{}", s1), "  5  4  3      ");

        let b2: Vec<u32> = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1].to_vec();
        Stack::set_capacity(b2.len());
        let s2 = Stack::new(&b2);
        assert_eq!(format!("{}", s2), " 10  9  8  7  6  5  4  3  2  1");

    }
}
