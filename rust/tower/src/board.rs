
use crate::stack::{Stack};
use std::fmt;

pub struct Board {
    peg: [Stack; 3],
    // peg: Stack,
}

impl Board {
    pub fn new(height: usize) -> Board {
        let disks: Vec<u32> = Vec::new();
        Stack::set_capacity(height);
        let mut board = Board {
            peg: [Stack::new(&disks), Stack::new(&disks), Stack::new(&disks)],
        };

        for i in (1..height+1).rev() {
            board.peg[0].push(i as u32);
        }

        board
    }

    fn can_move(&self, from: usize, to: usize) -> bool {
        if self.peg[from].height() == 0 {   // nothing on the 'from' peg?
            return false;
        }
        else if self.peg[to].height() == 0 {   // nothing on the 'to' peg?
            return true;
        }
        else if self.peg[to].top() > self.peg[from].top() {
            return true
        }

        false
    }

    fn move_disk(&mut self, from: usize, to: usize) {
        let disk_to_move  = self.peg[from].pop();
        self.peg[to].push(disk_to_move);
    }
}

impl fmt::Display for Board {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for p in &self.peg {
            write!(f, "{:3}|", p)?
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    #[allow(non_snake_case)]
    #[test]
    fn test_Board_new() {
        crate::stack::Stack::set_capacity(5);
        let b1 = crate::board::Board::new(5);
        assert_eq!(b1.peg[0].height(), 5);
        assert_eq!(b1.peg[1].height(), 0);
        assert_eq!(b1.peg[2].height(), 0);
        assert_eq!(format!("{}", b1), "  5  4  3  2  1|               |               |");
        let b2 = crate::board::Board::new(3);
        assert_eq!(b2.peg[0].height(), 3);
        assert_eq!(b2.peg[1].height(), 0);
        assert_eq!(b2.peg[2].height(), 0);
        assert_eq!(format!("{}", b2), "  3  2  1|         |         |");
    }

    #[allow(non_snake_case)]
    #[test]
    fn test_Board_movement() {
        let mut b1 = crate::board::Board::new(3);
        assert!(b1.can_move(0,1));
        b1.move_disk(0,1);

        assert_eq!(b1.peg[0].top(), Some(2));
        assert_eq!(b1.peg[0].height(), 2);
        assert_eq!(b1.peg[1].top(), Some(1));
        assert_eq!(b1.peg[1].height(), 1);

        assert!(!b1.can_move(0,1));
    }

    #[test]
    #[allow(non_snake_case)]
    #[should_panic(expected = "disk not smaller than top")]
    fn test_Board_illegal_move_disk() {
        let mut b1 = crate::board::Board::new(3);
        b1.move_disk(0,1);
        b1.move_disk(0,1);
   }

}
