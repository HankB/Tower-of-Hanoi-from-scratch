# Tower of Hanoi from scratch

Develop code to solve the Tower of Hanoi puzzle. The rules are stated succinctly at https://www.cs.sfu.ca/~tamaras/recursion/Rules_Towers_Hanoi.html

1. Move only one disk at a time.
1. A larger disk may not be placed on top of a smaller disk.
1. All disks, except the one being moved, must be on a peg.

## Motivation

I got my 7 year old grandson the Tower of Hanoi puzzle fir his birthday. He was more interested in the huge Lego set he also got so I worked on it with my 5 year old granddaughter. I explained the rules (not very concisely) and had a go at it. After a little while I gave up. I have no doubt that if I keep trying (and took notes) I could discover the pattern that would lead to a solution. Or I could cheat and look it up on the Internet. ;) As a developer I decided it would be more interesting to code something that would solve the puzzle.

## Usage

Each langiuage subdirectory includes a README with instructions to build, test and run.

## Contributing

If this project and/or my efforts interest you in any way, feel free to
contribute. It's a learning exercise for me and there are many out there who
can suggest improvements, either via PRs or issues. The closest I will get
to a CLA is to request that you submit any changes under the same permissive
license I (will) choose to avoid any ownership claims. Thanks!

## Planning

### Language

Undecided at this point. Almost any language should provide the capabilities to solve this though some would be more suitable.

### Strategy

The first cut will be a brute force. 

* For a given configuration, determine possible moves.
* Determine if any of the moves consists of a 'win.'
* Repeat for all configurations that result from possible moves.
* Discard any possible configurations that have already been explored. (There is the possibility that a sequence of moves could loop back to a previous pattern.)
* Report the sequence of moves that finished the puzzle.

#### Optional

* Continue the search to identify optimal and worst case solutions.

### Needs

* A representation of the disk denoting size.
* A representation of disks stacked on the posts.
* A way to connect configurations to one or more 'next' configurations.
* A way to iterate over all discovered configurations to look for matches.
* A way to report the winning solution. Perhaps a way to report trials along the way.
* A way to indicate a move of 0ne disk to another peg.
* A way to determine if the disk can be moved from one peg to another.

### Language - again

There also exists the opportunity to implement in several languages in order to explore them - both familiar and not. The first implementation should be in something familiar and that includes

* C - like a comfortable old shoe.
* C++ - Something I haven't used much lately. Is STL still used for collections? Algorithms?
* Perl - (5? 6?) 5 is 'an old shoe' and 6 is unknown to me.
* Python - (3, of course) The 'new friend.'
* Go - another new friend, but figuring out the directory hierarchy for this project gives me a headache.
* Java - one I haven't used much lately. Do I need/want a refresher?
* ASM - Ha ha!

\<mulling it over...> 

* Python! It would be good to learn more python data structures. Done! Python3 of course.
* C/C++! Initial effort is "C compiled with C++ compiler." Does C++ still provide better type safety. Areas for exploration include CMake, CTest and further TDD.
* Rust. Rust looks like an up and coming language about which I know the least. Getting started with that has been a bit of a slog but I'm actually making progress and have added some of my work to this project.

## Methodology

Stick with test driven development.

1. Write code that does nothing.
1. Write a test that fails.
1. Fix the code to pass.
1. Iterate, adding the required features.

NB: I don't strictly adhere to TDD but I find myself writing unit tests as I go. I get two benefits.

1. Instant gratification. Every test that passes gives me something "that works."
1. As I start to assemble the pieces, things generally go pretty well. (I just need to be careful writing tests. ;) )